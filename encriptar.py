#Previamente tener instalado python y cryptography pip install cryptography como administrador en la terminal

import cryptography
from cryptography.fernet import Fernet

#Generamos la clave en un archivo, esta se ejecuta UNA vez dentro de la carpeta para generar la llave
def crea_key():
    key = Fernet.generate_key()
    with open("key.key", "wb") as key_file:
        key_file.write(key)

#Funcion para cargar la clave siempre
def carga_key():
    return open("key.key", "rb").read()

#Funcion para encriptar
def encripta(archivo, key):
    f = Fernet(key)
    #Leemos el archivo a encriptar
    with open(archivo, "rb") as file:
        file_data = file.read()     
    #Se encripta el archivo leido
    encrypted_data = f.encrypt(file_data)
    #Se sustituye el archivo leido por uno nuevo ya encriptado
    with open(archivo, "wb") as file:
        file.write(encrypted_data)
        print("Encriptado: ",encrypted_data)

#Funcion para desencriptar
def desencripta(archivo, key):
    f = Fernet(key)
    #Leemos el archivo a desencriptar
    with open(archivo, "rb") as file:
        encrypted_data = file.read()
    #Se desencripta el archivo leido
    decrypted_data = f.decrypt(encrypted_data)
    #Se sustituye el archivo leido por uno nuevo desencriptado
    with open(archivo, "wb") as file:
        file.write(decrypted_data)
        print("Desencriptado: ",decrypted_data)


#Siempre se llama porque carga la llave que es necesaria
key = carga_key()
prueba = "cod.jpg"

#Prueba para encriptar
# encripta(prueba, key)
#Prueba para desencriptar
#desencripta(prueba,key)
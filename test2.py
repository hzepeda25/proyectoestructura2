from tkinter import *
import os

screen = None 
screen1 = None
screen2 = None
screen3 = None
screen4 = None
screen5 = None
screen6 = None
screen7 = None
screen8 = None

username = None
password = None 
username_entry = None
password_entry = None

raw_filename = None
raw_notes = None
raw_filename1 = None
raw_filename2 = None

username_verify = None
password_verify = None
username_entry1 = None
password_entry1 = None

def logout():
    screen3.destroy()

def save():
    filename = raw_filename.get()
    notes = raw_notes.get()

    data = open(filename, "w")
    data.write(notes)
    data.close()

    Label(screen4, text = "Saved!", fg = "green", font = ("Calibri", 11)).pack()

def create_notes():
    global screen4
    global raw_filename
    global raw_notes

    raw_filename = StringVar()
    raw_notes = StringVar()

    screen4 = Toplevel(screen)
    screen4.title("Info")
    screen4.geometry("300x250")
    Label(screen4, text = "Enter filename: ").pack()
    Entry(screen4, textvariable = raw_filename).pack()
    Label(screen4, text = "Enter notes: ").pack()
    Entry(screen4, textvariable = raw_notes).pack()

    Button(screen4, text = "Save", command = save).pack()

def view_notes1():
    global screen6

    filename1 = raw_filename1.get()
    data = open(filename1, "r")
    data1 = data.read()
    screen6 = Toplevel(screen)
    screen6.title("Notes")
    screen6.geometry("400x400")
    Label(screen6, text = data1).pack()

def view_notes():
    global screen5
    global raw_filename1

    screen5 = Toplevel(screen)
    screen5.title("Info")
    screen5.geometry("250x250")

    all_files = os.listdir()
    Label(screen5, text = "Please use one of the filenames below: ").pack()
    Label(screen5, text = all_files).pack()

    raw_filename1 = StringVar()
    Entry(screen5, textvariable = raw_filename1).pack()
    Button(screen5, text = "OK", command = view_notes1).pack()

def delete_notes1():
    global screen8

    filename2 = raw_filename2.get()
    os.remove(filename2)

    screen8 = Toplevel(screen)
    screen8.title("Notes")
    screen8.geometry("400x400")
    Label(screen8, text = filename2 + " has been removed!").pack()

def delete_notes():
    global screen7
    global raw_filename2

    screen7 = Toplevel(screen)
    screen7.title("Info")
    screen7.geometry("250x250")

    all_files = os.listdir()
    Label(screen7, text = "Please use one of the filenames below: ").pack()
    Label(screen7, text = all_files).pack()

    raw_filename2 = StringVar()
    Entry(screen7, textvariable = raw_filename2).pack()
    Button(screen7, text = "Delete", command = delete_notes1).pack()

def session():
    global screen3
    screen3 = Toplevel(screen)
    screen3.title("Dashboard")
    screen3.geometry("400x400")
    Label(screen3, text = "Welcome to Dashboard!").pack()
    Button(screen3, text = "Create note", command = create_notes).pack()
    Button(screen3, text = "View note", command = view_notes).pack()
    Button(screen3, text = "Delete note", command = delete_notes).pack()

def login_success():
    session()

# Función que registra un nuevo usuario
def register_user():

    username_info = username.get()
    password_info = password.get()

    file = open(username_info, "w")
    file.write(username_info + "\n")
    file.write(password_info + "\n")
    file.close()

    username_entry.delete(0, END)
    password_entry.delete(0, END)

    Label(screen1, text = "Registration Sucessful!", fg = "green", font = ("Calibri", 11)).pack()

# Función que verifica el usuario
def login_verify():

    username1 = username_verify.get()
    password1 = password_verify.get()

    username_entry1.delete(0, END)
    password_entry1.delete(0, END)

    list_of_files = os.listdir()

    if username1 in list_of_files:
        file1 = open(username1, "r")
        verify = file1.read().splitlines()
        if password1 in verify:
            Label(screen2, text = "Login success!", fg = "green", font = ("Calibri", 11)).pack()
            login_success()
        else:
            Label(screen2, text = "Password not recognized.", fg = "red", font = ("Calibri", 11)).pack()
    else:
        Label(screen2, text ="User not found.", fg = "red", font = ("Calibri", 11)).pack()

# Función que obtiene los datos de un nuevo usuario
def register():

    global screen1

    screen1 = Toplevel(screen)
    screen1.title("Register")
    screen1.geometry("300x250")

    global username
    global password
    global username_entry
    global password_entry

    username = StringVar()
    password = StringVar()

    Label(screen1, text = "Please enter details below").pack()

    Label(screen1, text = "").pack()

    Label(screen1, text = "Username * ").pack()
    username_entry = Entry(screen1, textvariable = username)
    username_entry.pack()

    Label(screen1, text = "Password * ").pack()
    password_entry = Entry(screen1, textvariable = password, show = "*")
    password_entry.pack()

    Label(screen1, text = "").pack() 

    Button(screen1, text = "Register", width = "10", height = "1", command = register_user).pack()

# Función que obtiene los datos del usuario
def login():

    global screen2

    screen2 = Toplevel(screen)
    screen2.title("Log in")
    screen2.geometry("300x250")

    global username_verify
    global password_verify
    global username_entry1
    global password_entry1

    username_verify = StringVar()
    password_verify = StringVar()

    Label(screen2, text = "Please enter details below").pack()
    
    Label(screen2, text = "").pack()

    Label(screen2, text = "Username * ").pack()
    username_entry1 = Entry(screen2, textvariable = username_verify)
    username_entry1.pack()

    Label(screen2, text = "Password * ").pack()
    password_entry1 = Entry(screen2, textvariable = password_verify, show = "*")
    password_entry1.pack()

    Label(screen2, text = "").pack() 

    Button(screen2, text = "Log in", width = "10", height = "1", command = login_verify).pack()

    print("Login session started")

# Función que inicializa las propiedades de la pantalla
def main_screen():
    
    global screen
    screen = Tk()
    screen.geometry("300x250")
    screen.title("Notes 1.0")
    Label(text = "Notes 1.0", bg = "grey", width = "300", height = "2", font = ("Calibri", 13)).pack()
    Label(text = "").pack()
    Button(text = "Login", height = "2", width = "30", command = login).pack()
    Label(text = "").pack()
    Button(text = "Register", height = "2", width = "30", command = register).pack()

    screen.mainloop() 

main_screen()
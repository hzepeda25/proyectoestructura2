from cryptography.fernet import Fernet
import os
import eel
from tkinter import Tk
from tkinter.filedialog import askopenfilename

root = Tk()
# Tk().withdraw()

apps = []


# exposing getLoginInfo to javascript
eel.init('client/htmlViews')


@eel.expose
def session():
    eel.switchToLandingPage()


def login_success():
    session()


@eel.expose
def register_user(user, pswd):
    file = open(user, "w")
    file.write(user + "\n")
    file.write(pswd + "\n")
    file.close()
    msg = user + " was succesfully created as a user."
    eel.showSuccessAlert(msg)
    # alert login and register succesful


@eel.expose
def getLoginInfo(user, pswd):
    list_of_files = os.listdir()
    if user in list_of_files:
        file1 = open(user, "r")
        verify = file1.read().splitlines()
        if pswd in verify:
            # Alert Label(screen2, text = "Login success!", fg = "green", font = ("Calibri", 11)).pack()
            login_success()
        else:
            # Alert Label(screen2, text = "Password not recognized.", fg = "red", font = ("Calibri", 11)).pack()
            eel.showErrorAlert("Wrong Password")
    else:
        eel.showErrorAlert("User not found")
        # Alert Label(screen2, text ="User not found.", fg = "red", font = ("Calibri", 11)).pack()


@eel.expose
def save(fileName, fileText, isEditing):
    data = open(fileName, "w")
    data.write(fileText)
    data.close()
    eel.showSuccessAlert(" File succesfully saved.")

    if (isEditing):
        eel.showSpecFile(fileName, fileText)
        eel.showSuccessAlert(" File succesfully edited.")


@eel.expose
def view_notes():

    # Open a file in any location
    filename = askopenfilename()
    data = open(filename, "r")
    data1 = data.read()
    eel.showSpecFile(filename, data1)


def delete_notes():
    global raw_filename2
    all_files = os.listdir()

  # info 250x250 screen text input:  "Please use one of the filenames below: ", bind: all_files
  # button"delete", bind: delete_notes1


encrypted = None
key = None


@eel.expose
def create_key():

    global key
    key = Fernet.generate_key()
    with open("key.key", "wb") as key_file:
        key_file.write(key)


def get_key():
    global key
    # Get the key from the file
    file = open('key.key', 'rb')
    key = file.read()  # The key will be type bytes
    file.close()


@eel.expose
def encrypt():

    global encrypted

    create_key()

    # Open the file to encrypt
    filename = askopenfilename()
    with open(filename, 'rb') as f:
        data = f.read()

    fernet = Fernet(key)
    encrypted = fernet.encrypt(data)

    # Write the encrypted file
    with open(filename, 'wb') as f:
        f.write(encrypted)
        eel.showSuccessAlert(" File succesfully encrypted.")
        eel.showCryptedFile(filename)


@eel.expose
def decrypt():
    # Open the file to decrypt
    get_key()
    filename = askopenfilename()

    with open(filename, 'rb') as f:
        data = f.read()

    fernet = Fernet(key)
    encrypted = fernet.decrypt(data)

    # Write the encrypted file
    with open(filename, 'wb') as f:
        f.write(encrypted)
        eel.showSuccessAlert(" File succesfully decrypted.")
        eel.showDecryptedFile(filename)


eel.start('login.html', port=8080, size=(1000, 600))

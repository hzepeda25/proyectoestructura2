function expand(el) {
    let expandDiv_node = document.getElementById(el.dataset.expand);
    if (el.style.transform !== "rotate(0deg)") {
        el.style.transform = 'rotate(0deg)';
        if (el.dataset.display !== undefined) {
            expandDiv_node.style.display = el.dataset.display;
        } else {
            expandDiv_node.style.display = 'block';
        }
    } else {
        el.style.transform = 'rotate(180deg)';
        expandDiv_node.style.display = 'none';
    }
}

function catchFileToEncrypt() {
    eel.encrypt();
}

function catchFileToDecrypt() {
    eel.decrypt();
}

function catchFileToSave() {
    var fileName = document.getElementById("fileText").value;
    var fileText = document.getElementById("textNotesArea").value;
    var isEditing = false;

    eel.save(fileName, fileText, isEditing);
}

function catchFileToEdit() {
    var fileName = document.getElementById("fileToShowText").value;
    var fileText = document.getElementById("fileShowingTextArea").value;
    var isEditing = true;
    eel.save(fileName, fileText, isEditing);
}

function catchFileToShow(fileName, data) {
    eel.view_notes();
}

function searchFile() {
    var fileName = document.getElementById("fileToEncrypt").value;

    eel.search_file(fileName);
}